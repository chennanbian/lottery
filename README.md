go开发高并发企业级抽奖系统

借助go web框架 iris 开发的一个高并发抽奖系统，系统分3大模块：

展示层，有前端页面和后台管理页面。前端页面（实现登录注销，抽奖，查看奖品功能），后端管理页面（实现登录注销，奖品库存显示，增减库存，更改发奖计划，IP黑名单，用户黑名单，中奖纪录功能）
框架层（iris框架，thrift框架，业务实现）
资源层（mysql存储数据，redis缓存优化项目速度）
主要难点有2个，一是抽奖服务的实现，因为是并发抽奖，要保证多能多发。二是定时自动给奖品池填充奖品，根据奖品库存生成发奖计划的服务

最后用 thrift rpc 框架将抽奖功能做封装，再编写对应的客户端实现远程调用

![](https://images.gitee.com/uploads/images/2019/0904/213854_0f27fa7b_5249696.jpeg "TIM截图20190904213811.jpg")
![](https://images.gitee.com/uploads/images/2019/0904/213735_66d28432_5249696.jpeg "TIM截图20190904213654.jpg")
![](https://images.gitee.com/uploads/images/2019/0904/213542_574ecc9c_5249696.jpeg "TIM截图20190904213452.jpg")
![](https://images.gitee.com/uploads/images/2019/0904/213624_7b227cb7_5249696.jpeg "TIM截图20190904213544.jpg")
![](https://images.gitee.com/uploads/images/2019/0904/213818_97b3914d_5249696.jpeg "TIM截图20190904213735.jpg")
![前端抽奖页面](https://images.gitee.com/uploads/images/2019/0904/213947_ffdc73c6_5249696.jpeg "12-3.jpg")
![后端奖品管理页面](https://images.gitee.com/uploads/images/2019/0904/214014_8feac3fb_5249696.jpeg "在这里输入图片标题")
![](https://images.gitee.com/uploads/images/2019/0904/214057_fe200ef5_5249696.jpeg "12-6.jpg")
![](https://images.gitee.com/uploads/images/2019/0904/214108_b450ed9a_5249696.jpeg "12-7.jpg")
![](https://images.gitee.com/uploads/images/2019/0904/214117_787aa3c5_5249696.jpeg "12-8.jpg")
