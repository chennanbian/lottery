package controllers

import (
	"imooc.com/demo-lottery/conf"
	"imooc.com/demo-lottery/models"
	"imooc.com/demo-lottery/services"
)

func (api *LuckyApi) prize(prizeCode int,
	limitBlack bool) *models.ObjGiftPrize {

	var prizeGift *models.ObjGiftPrize
	giftList := services.NewGiftService().GetAllUse(true)
	for _, gift := range giftList {
		if gift.PrizeCodeA <= prizeCode &&
			gift.PrizeCodeB >= prizeCode {
			// 中奖编码满足条件，可以中奖
			if !limitBlack || gift.Gtype < conf.GtypeGiftSmall {
				prizeGift = &gift
				break
			}
		}
	}
	return prizeGift
}
