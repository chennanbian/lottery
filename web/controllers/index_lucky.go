package controllers

import (
	"imooc.com/demo-lottery/comm"
)

// http:localhost:8080/lucky
func (c *IndexController) GetLucky() map[string]interface{} {
	rs := make(map[string]interface{})
	// 默认值
	rs["code"] = 0
	rs["msg"] = ""

	// 1 验证登陆用户
	loginuser := comm.GetLoginUser(c.Ctx.Request())
	if loginuser == nil || loginuser.Uid < 1 {
		rs["code"] = 101
		rs["msg"] = "请先登陆，再来抽奖"
		return rs
	}

	/*// 2 用户抽奖分布式锁定
	ok := utils.LockLucky(loginuser.Uid)
	if ok {
		defer utils.UnlockLucky(loginuser.Uid)
	} else {
		rs["code"] = 102
		rs["msg"] = "正在抽奖，请稍后重试"
		return rs
	}

	// 3 验证用户今日抽奖次数
	// 这个逻辑的前提是认为缓存不可考，如果rds的逻辑设计得可靠，就不用走下面else的步骤
	userDayNum := utils.IncrUserLuckyNum(loginuser.Uid)
	if userDayNum > conf.UserPrizeMax {
		rs["code"] = 103
		rs["msg"] = "今日抽奖次数已用完，请明天再来"
		return rs
	} else {
		ok = c.checkUserday(loginuser.Uid, userDayNum)
		if !ok {
			rs["code"] = 103
			rs["msg"] = "今日抽奖次数已用完，请明天再来"
			return rs
		}
	}

	// 4 验证ip今日参与次数
	ip := comm.ClientIP(c.Ctx.Request())
	ipDayNum := utils.IncrIpLuckyNum(ip)
	if ipDayNum > conf.IpLimitMax {
		rs["code"] = 104
		rs["msg"] = "相同IP参与次数太多，明天再来参与吧"
		return rs
	}

	limitBlack := false // 黑名单,默认不在黑名单
	if ipDayNum > conf.IpPrizeMax {
		limitBlack = true
	}
	// 5 验证ip黑名单
	var blackipInfo *models.LtBlackip
	if !limitBlack {
		ok, blackipInfo = c.checkBlackip(ip)
		if !ok {
			fmt.Println("黑名单中的IP:", ip, limitBlack)
			limitBlack = true
		}
	}

	// 6 验证用户黑名单
	var userInfo *models.LtUser
	if !limitBlack {
		ok, userInfo = c.checkBlackUser(loginuser.Uid)
		if !ok {
			fmt.Println("黑名单中的用户:", loginuser.Uid, limitBlack)
			limitBlack = true
		}
	}

	// 7 验证抽奖编码
	prizeCode := comm.Random(conf.MaxPrizeCode)

	// 8 匹配奖品是否中奖
	prizeGift := c.prize(prizeCode, limitBlack)
	if prizeGift == nil ||
		prizeGift.PrizeNum < 0 ||
		(prizeGift.PrizeNum > 0 && prizeGift.LeftNum <= 0) {
		rs["code"] = 205
		rs["msg"] = "很遗憾，没有中奖，请再次尝试"
		return rs
	}

	// 9 有限奖品发放
	if prizeGift.PrizeNum > 0 {
		if utils.GetGiftPoolNum(prizeGift.Id) <= 0 {
			rs["code"] = 206
			rs["msg"] = "很遗憾，没有中奖，请再次尝试"
			return rs
		}
		ok := utils.PrizeGift(prizeGift.Id, prizeGift.LeftNum)
		// 如果 ok == true，说明奖品扣除成功，继续下一步
		if !ok {
			rs["code"] = 207
			rs["msg"] = "很遗憾，没有中奖，请再次尝试"
			return rs
		}
	}

	// 10 不同编码的优惠券发放
	if prizeGift.Gtype == conf.GtypeCodeDiff {
		code := utils.PrizeCodeDiff(prizeGift.Id, c.ServiceCode)
		if code == "" {
			rs["code"] = 208
			rs["msg"] = "很遗憾，没有中奖，请再次尝试"
			return rs
		}
	}

	// 11 记录中奖信息
	result := models.LtResult{
		GiftId:     prizeGift.Id,
		GiftName:   prizeGift.Title,
		GiftType:   prizeGift.Gtype,
		Uid:        loginuser.Uid,
		Username:   loginuser.Username,
		PrizeCode:  prizeCode,
		GiftData:   prizeGift.Gdata,
		SysCreated: comm.NowUnix(),
		SysIp:      ip,
		SysStatus:  0,
	}
	err := c.ServiceResult.Create(&result)
	if err != nil {
		log.Println("index_lucky.GetLucky ServiceResult.Greate ",
			result, ", error=", err)
		rs["code"] = 209
		rs["msg"] = "很遗憾，没有中奖，请再次尝试"
		return rs
	}

	// 12 返回抽奖结果
	if prizeGift.Gtype == conf.GtypeGiftLarge {
		// 如果是实物大奖，将用户，ip地址 设置黑名单一段时间
		c.prizeLarge(ip, loginuser, userInfo, blackipInfo)

	}
	rs["gift"] = prizeGift

	return rs*/

	ip := comm.ClientIP(c.Ctx.Request())
	api := &LuckyApi{}
	code, msg, gift := api.luckyDo(loginuser.Uid, loginuser.Username, ip)
	rs["code"] = code
	rs["msg"] = msg
	rs["gift"] = gift
	return rs
}
