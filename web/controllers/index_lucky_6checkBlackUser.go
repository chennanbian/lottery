package controllers

import (
	"imooc.com/demo-lottery/models"
	"imooc.com/demo-lottery/services"
	"time"
)

func (api *LuckyApi) checkBlackUser(uid int) (bool, *models.LtUser) {
	info := services.NewUserService().Get(uid)
	if info != nil && info.Blacktime > int(time.Now().Unix()) {
		// 黑名单存在且时间未过期
		return false, info
	}
	return true, info
}
