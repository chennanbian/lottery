package controllers

import (
	"fmt"
	"imooc.com/demo-lottery/conf"
	"imooc.com/demo-lottery/models"
	"imooc.com/demo-lottery/services"
	"imooc.com/demo-lottery/web/utils"
	"log"
	"strconv"
	"time"
)

func (api *LuckyApi) checkUserday(uid int, num int64) bool {
	userdayService := services.NewUserdayService()
	userdayInfo := userdayService.GetUserToday(uid)
	if userdayInfo != nil && userdayInfo.Uid == uid {
		// 今天存在抽奖记录
		if userdayInfo.Num >= conf.UserPrizeMax {
			// 如果rds的数量小于数据库的数量，更新rds数量，以数据库为准
			if int(num) < userdayInfo.Num {
				utils.InitUserLuckyNum(uid, int64(userdayInfo.Num))
			}
			return false
		} else {
			userdayInfo.Num++
			// 如果rds的数量小于数据库的数量，更新rds数量，以数据库为准
			if int(num) < userdayInfo.Num {
				utils.InitUserLuckyNum(uid, int64(userdayInfo.Num))
			}
			err := userdayService.Update(userdayInfo, nil)
			if err != nil {
				log.Println("index_lucky_check_userday "+
					"Service.Update error=", err)
			}
		}
	} else {
		// 创建今天的用户参与记录
		y, m, d := time.Now().Date()
		strDay := fmt.Sprintf("%d%02d%02d", y, m, d)
		day, _ := strconv.Atoi(strDay)
		userdayInfo = &models.LtUserday{
			Uid:        uid,
			Day:        day,
			Num:        1,
			SysCreated: int(time.Now().Unix()),
		}
		err := userdayService.Create(userdayInfo)
		if err != nil {
			log.Println("index_lucky_check_userday "+
				"Service.Create error=", err)
		}
		// 创建记录后，初始化rds
		utils.InitUserLuckyNum(uid, 1)
	}
	return true
}
