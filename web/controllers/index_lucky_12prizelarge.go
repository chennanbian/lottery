package controllers

import (
	"imooc.com/demo-lottery/comm"
	"imooc.com/demo-lottery/models"
	"imooc.com/demo-lottery/services"
)

func (api *LuckyApi) prizeLarge(ip string,
	uid int, username string, // 登陆用户
	// 用户黑名单 ip黑名单
	userinfo *models.LtUser, blackipInfo *models.LtBlackip) {

	userService := services.NewUserService()
	blackipService := services.NewBlackipService()
	nowTime := comm.NowUnix()
	blackTime := 30 * 86400
	// 更新用户黑名单信息
	if userinfo == nil || userinfo.Id <= 0 {
		userinfo = &models.LtUser{
			Id:         uid,
			Username:   username,
			Blacktime:  nowTime + blackTime,
			SysCreated: nowTime,
			SysIp:      ip,
		}
		userService.Create(userinfo)
	} else {
		userinfo = &models.LtUser{
			Id:         uid,
			Username:   username,
			Blacktime:  nowTime + blackTime,
			SysUpdated: nowTime,
		}
		userService.Update(userinfo, nil)
	}

	// 更新ip黑名单
	if blackipInfo == nil || blackipInfo.Id <= 0 {
		blackipInfo = &models.LtBlackip{
			Ip:         ip,
			Blacktime:  nowTime + blackTime,
			SysCreated: nowTime,
		}
		blackipService.Create(blackipInfo)
	} else {
		blackipInfo.Blacktime = nowTime + blackTime
		blackipInfo.SysUpdated = nowTime
		blackipService.Update(blackipInfo, nil)
	}
}
