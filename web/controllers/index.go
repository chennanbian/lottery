package controllers

import (
	"fmt"
	"github.com/kataras/iris"
	"imooc.com/demo-lottery/comm"
	"imooc.com/demo-lottery/models"
	"imooc.com/demo-lottery/services"
)

type IndexController struct {
	Ctx            iris.Context
	ServiceUser    services.UserService
	ServiceGift    services.GiftService
	ServiceCode    services.CodeService
	ServiceResult  services.ResultService
	ServiceUserday services.UserdayService
	ServiceBlackip services.BlackipService
}

// http://localhost:8080
func (c *IndexController) Get() string {
	c.Ctx.Header("Content-Type", "text/html")
	return "welcome to Go抽奖系统,<a href='/public/index.html'>开始抽奖</a>"
}

// http://localhost:8080/gifts
func (c *IndexController) GetGifts() map[string]interface{} {
	rs := make(map[string]interface{}) //最后输出json
	rs["code"] = 0
	rs["msg"] = ""
	datalist := c.ServiceGift.GetAll(true)
	list := make([]models.LtGift, 0)
	for _, data := range datalist {
		if data.SysStatus == 0 {
			list = append(list, data)
		}
	}

	rs["gifts"] = list
	return rs
}

// http://localhost:8080/newprize
func (c *IndexController) GetNewprize() map[string]interface{} {
	rs := make(map[string]interface{}) //最后输出json
	rs["code"] = 0
	rs["msg"] = ""
	gifts := c.ServiceGift.GetAll(true)
	giftIds := []int{}
	for _, data := range gifts {
		// 虚拟券或者实物奖才需要放到外部榜单中展示
		if data.Gtype > 1 {
			giftIds = append(giftIds, data.Id)
		}
	}
	list := c.ServiceResult.GetNewPrize(50, giftIds)
	rs["prize_list"] = list
	return rs
}

// 登陆
func (c *IndexController) GetLogin() {
	// 每次登陆就随机生成1个uid，就不去作用户密码这些了
	uid := comm.Random(100000)
	loginuser := &models.ObjLoginuser{
		Uid:      uid,
		Username: fmt.Sprintf("admin-%d", uid),
		Now:      comm.NowUnix(),
		Ip:       comm.ClientIP(c.Ctx.Request()),
	}
	// 登陆
	comm.SetLoginuser(c.Ctx.ResponseWriter(), loginuser)
	// 登陆成功后作一次跳转
	comm.Redirect(c.Ctx.ResponseWriter(),
		"/public/index.html?from=login")
}

// 退出用户
func (c *IndexController) GetLogout() {
	// 清空cookie
	comm.SetLoginuser(c.Ctx.ResponseWriter(), nil)
	// 跳回首页
	comm.Redirect(c.Ctx.ResponseWriter(),
		"/public/index.html?from=logout")
}
