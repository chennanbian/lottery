package controllers

import (
	"fmt"
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	"imooc.com/demo-lottery/comm"
	"imooc.com/demo-lottery/conf"
	"imooc.com/demo-lottery/models"
	"imooc.com/demo-lottery/services"
	"imooc.com/demo-lottery/web/utils"
	"strings"
)

type AdminCodeController struct {
	Ctx            iris.Context
	ServiceUser    services.UserService
	ServiceGift    services.GiftService
	ServiceCode    services.CodeService
	ServiceResult  services.ResultService
	ServiceUserday services.UserdayService
	ServiceBlackip services.BlackipService
}

/*
type View struct {
	Name   string  //文件名，如 hello/index.html
	Layout string  //布局
	Data   interface{} // map or a custom struct.  //数据
	Code   int     //错误码
	Err    error   //err
}*/

// 获取优惠券信息
func (c *AdminCodeController) Get() mvc.Result {
	giftId := c.Ctx.URLParamIntDefault("gift_id", 0)
	page := c.Ctx.URLParamIntDefault("page", 1)
	size := 100
	pagePrev := ""
	pageNext := ""
	// 数据列表
	var datalist []models.LtCode
	var total int = 0
	var dbNum, cacheNum int
	if giftId > 0 {
		datalist = c.ServiceCode.Search(giftId)
		dbNum, cacheNum = utils.GetCacheCodeNum(giftId, c.ServiceCode)
	} else {
		datalist = c.ServiceCode.GetAll(page, size)
	}
	total = (page-1)*size + len(datalist)
	// 小优化，如果这一步成立，就不需要再去查数据库
	if len(datalist) >= size {
		if giftId > 0 {
			total = int(c.ServiceCode.CountByGift(giftId))
		} else {
			total = int(c.ServiceCode.CountAll())
		}
		pageNext = fmt.Sprintf("%d", page+1)
	}
	if page > 1 {
		pagePrev = fmt.Sprintf("%d", page-1)
	}

	return mvc.View{
		Name: "admin/code.html",
		Data: iris.Map{
			"Title":    "管理后台",
			"Channel":  "code", //频道
			"GiftId":   giftId,
			"Datalist": datalist,
			"Total":    total,
			"PagePrev": pagePrev,
			"PageNext": pageNext,
			"CodeNum":  dbNum,
			"CacheNum": cacheNum,
		},
		Layout: "admin/layout.html",
	}
}

func (c *AdminCodeController) PostImport() {
	giftId := c.Ctx.URLParamIntDefault("gift_id", 0)
	fmt.Println("PostImport giftId=", giftId)
	if giftId < 1 {
		c.Ctx.Text("没有指定奖品ID，无法进行导入，" +
			"<a href='' onclick=history.go(-1);return false;>返回</a>")
		return
	}
	gift := c.ServiceGift.Get(giftId, false)
	if gift == nil || gift.Gtype != conf.GtypeCodeDiff {
		c.Ctx.Text("奖品信息不存在或者奖品类型不是差异化优惠券，无法进行导入，" +
			"<a href='' onclick=history.go(-1);return false;>返回</a>")
		return
	}
	codes := c.Ctx.PostValue("codes")
	now := comm.NowUnix()
	list := strings.Split(codes, "\n")
	sucNum := 0
	errNum := 0
	for _, code := range list {
		code = strings.TrimSpace(code) //去除空格
		if code != "" {
			data := &models.LtCode{
				GiftId:     giftId,
				Code:       code,
				SysCreated: now,
			}
			err := c.ServiceCode.Create(data)
			if err != nil {
				errNum++
			} else {
				//成功导入数据库，下一步还需要导入缓存
				ok := utils.ImportCacheCodes(giftId, code)
				if ok {
					sucNum++
				} else {
					errNum++
				}
			}
		}
	}
	c.Ctx.HTML(fmt.Sprintf("成功导入%d条，导入失败%d条，"+
		"<a href='/admin/code?gift_id=%d'>返回</a>", sucNum, errNum, giftId))
}

func (c *AdminCodeController) GetDelete() mvc.Result {
	id, err := c.Ctx.URLParamInt("id")
	if err == nil {
		c.ServiceCode.Delete(id)
	}
	refer := c.Ctx.GetHeader("Referer")
	// 如果为空，返回默认的页面，不为空，返回操作的页面
	if refer == "" {
		refer = "/admin/code"
	}
	return mvc.Response{
		Path: refer, //如果不为空且err==nil，会做跳转
	}
}

func (c *AdminCodeController) GetReset() mvc.Result {
	id, err := c.Ctx.URLParamInt("id")
	if err == nil {
		c.ServiceCode.Update(&models.LtCode{Id: id, SysStatus: 0},
			[]string{"sys_status"})
	}
	refer := c.Ctx.GetHeader("Referer")
	if refer == "" {
		refer = "/admin/code"
	}
	return mvc.Response{
		Path: refer, //如果不为空且err==nil，会做跳转
	}
}

// 重新整理优惠券的数据，如果是本地服务，也需要启动时加载
func (c *AdminCodeController) GetRecahe() {
	refer := c.Ctx.GetHeader("Referer")
	if refer == "" {
		refer = "/admin/code"
	}
	id, err := c.Ctx.URLParamInt("id")
	if id < 1 || err != nil {
		rs := fmt.Sprintf("没有指定优惠券所属的奖品id, "+
			"<a href='%s'>返回</a>", refer)
		c.Ctx.HTML(rs)
		return
	}
	sucNum, errNum := utils.RecacheCodes(id, c.ServiceCode)

	rs := fmt.Sprintf("sucNum=%d, errNum=%d, "+
		"<a href='%s'>返回</a>", sucNum, errNum, refer)
	c.Ctx.HTML(rs)
}
