package controllers

import (
	"imooc.com/demo-lottery/models"
	"imooc.com/demo-lottery/services"
	"time"
)

func (api *LuckyApi) checkBlackip(ip string) (bool, *models.LtBlackip) {
	info := services.NewBlackipService().GetByIp(ip)
	if info == nil || info.Ip == "" {
		return true, nil
	}
	if info.Blacktime > int(time.Now().Unix()) {
		// 黑名单存在并且未过期
		return false, info
	}
	return true, info
}
