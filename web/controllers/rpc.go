/**
 * thrift的rpc服务端实现
 * http://localhost:8080/rpc/
 */
package controllers

import (
	"context"
	"fmt"
	"github.com/apache/thrift/lib/go/thrift"
	"github.com/kataras/iris"
	"github.com/kataras/iris/core/errors"
	"imooc.com/demo-lottery/comm"
	"imooc.com/demo-lottery/rpc"
	"imooc.com/demo-lottery/services"
	"io/ioutil"
	"log"
	"math"
	"regexp"
	"time"
)

type RpcController struct {
	Ctx            iris.Context
	ServiceUser    services.UserService
	ServiceGift    services.GiftService
	ServiceCode    services.CodeService
	ServiceResult  services.ResultService
	ServiceUserday services.UserdayService
	ServiceBlackip services.BlackipService
}

type rpcServer struct{}

func (serv *rpcServer) DoLucky(ctx context.Context, uid int64,
	username string, ip string, now int64, app string,
	sign string) (*rpc.DataResult_, error) {

	// 校验参数是否合法
	err := serv.checkParams(uid, username, ip, now, app, sign)
	if err != nil {
		return nil, err
	}

	// 业务逻辑
	api := &LuckyApi{}
	rs := &rpc.DataResult_{}
	code, msg, gift := api.luckyDo(int(uid), username, ip)

	prizeGift := &rpc.DataGiftPrize{}
	if gift != nil && gift.Id > 0 {
		prizeGift = &rpc.DataGiftPrize{
			ID:           int64(gift.Id),
			Title:        gift.Title,
			Img:          gift.Img,
			Displayorder: int64(gift.Displayorder),
			Gtype:        int64(gift.Gtype),
			Gdata:        gift.Gdata,
		}
	}
	if code > 0 {
		return nil, errors.New(msg)
	} else {
		rs.Code = int64(code)
		rs.Msg = msg
		rs.Gift = prizeGift
		return rs, nil
	}
}

func (serv *rpcServer) MyPrizeList(ctx context.Context, uid int64,
	username string, ip string, now int64, app string,
	sign string) ([]*rpc.DataGiftPrize, error) {

	// 校验参数是否合法
	err := serv.checkParams(uid, username, ip, now, app, sign)
	if err != nil {
		return nil, err
	}

	// 业务逻辑
	list := services.NewResultService().
		SearchByUser(int(uid), 1, 100)
	rData := make([]*rpc.DataGiftPrize, len(list))
	for i, data := range list {
		info := &rpc.DataGiftPrize{
			ID:           int64(data.Id),
			Title:        data.GiftName,
			Img:          "",
			Displayorder: 0,
			Gtype:        int64(data.GiftType),
			Gdata:        data.GiftData,
		}
		rData[i] = info
	}

	return rData, nil
}

// 检验参数是否正确
func (serv *rpcServer) checkParams(uid int64, username string,
	ip string, now int64, app string, sign string) error {

	if uid < 1 {
		return errors.New("uid参数不正确")
	}
	str := fmt.Sprintf("uid=%d&username=%s&ip=%s&now=%d&app=%s",
		uid, username, ip, now, app)
	usign := comm.CreateSign(str)
	if sign != usign {
		return errors.New("sign签名参数不正确")
	}
	if now > math.MaxInt32 {
		// 纳秒时间
		nowt := time.Now().UnixNano()
		// 如果超过10秒
		if nowt > now+10*100000000 {
			return errors.New("now参数不正确")
		}
	} else {
		// 秒钟 unix时间戳
		nowt := time.Now().Unix()
		if nowt > now+10 {
			return errors.New("now参数不正确")
		}
	}
	return nil
}

// rpc 和 iris 结合的通用代码
// http://localhost:8080/rpc
func (c *RpcController) Post() {
	var (
		inProtocol  *thrift.TJSONProtocol //序列化和反序列化协议
		outProtocol *thrift.TJSONProtocol //序列化和反序列化协议
		inBuffer    thrift.TTransport     //传输协议
		outBuffer   thrift.TTransport     //传输协议
	)
	// 缓冲区，内容从iris中获取
	inBuffer = thrift.NewTMemoryBuffer()
	// iris的请求转换为thrift格式
	body, err := ioutil.ReadAll(c.Ctx.Request().Body)
	if err != nil {
		log.Println(err)
		return
	}
	// 转换处理: 转换回正常的json字符串
	body = convertReqBody(body)
	// 读取进去
	inBuffer.Write(body)
	if inBuffer != nil {
		defer inBuffer.Close()
	}

	outBuffer = thrift.NewTMemoryBuffer()
	if outBuffer != nil {
		defer outBuffer.Close()
	}

	// 拿到数据后再转换成需要的input参数
	inProtocol = thrift.NewTJSONProtocol(inBuffer)
	outProtocol = thrift.NewTJSONProtocol(outBuffer)
	// thrift服务，抽奖服务
	var serv rpc.LuckyService = &rpcServer{}
	// 将自己定义的rpcService对象传进去
	process := rpc.NewLuckyServiceProcessor(serv)
	// 实际的处理各个远程方法调用
	process.Process(c.Ctx.Request().Context(), inProtocol, outProtocol)

	// 最后的头信息和信息的输出
	out := make([]byte, outBuffer.RemainingBytes())
	outBuffer.Read(out)
	c.Ctx.ResponseWriter().WriteHeader(iris.StatusOK)
	c.Ctx.ResponseWriter().Write(out)
}

func convertReqBody(body []byte) []byte {
	reg1 := regexp.MustCompile("\\\\\"")
	reg2 := regexp.MustCompile("\"\"")
	reg3 := regexp.MustCompile("\"{")
	reg4 := regexp.MustCompile("}\"")
	reg5 := regexp.MustCompile("\"\\[")
	reg6 := regexp.MustCompile("]\"")
	for reg1.Find(body) != nil {
		body = reg1.ReplaceAll(body, []byte("\""))
		body = reg2.ReplaceAll(body, []byte("\""))
	}
	body = reg3.ReplaceAll(body, []byte("{"))
	body = reg4.ReplaceAll(body, []byte("}"))
	body = reg5.ReplaceAll(body, []byte("["))
	body = reg6.ReplaceAll(body, []byte("]"))

	return body
}
