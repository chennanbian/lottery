package controllers

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	"imooc.com/demo-lottery/services"
)

type AdminController struct {
	Ctx            iris.Context
	ServiceUser    services.UserService
	ServiceGift    services.GiftService
	ServiceCode    services.CodeService
	ServiceResult  services.ResultService
	ServiceUserday services.UserdayService
	ServiceBlackip services.BlackipService
}

/*
type View struct {
	Name   string  //文件名，如 hello/index.html
	Layout string  //布局
	Data   interface{} // map or a custom struct.  //数据
	Code   int     //错误码
	Err    error   //err
}*/

func (c *AdminController) Get() mvc.Result {
	return mvc.View{
		Name: "admin/index.html",
		Data: iris.Map{
			"Title":   "管理后台",
			"Channel": "", //频道
		},
		Layout: "admin/layout.html",
	}
}
