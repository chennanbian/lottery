package main

import (
	"fmt"
	"imooc.com/demo-lottery/bootstrap"
	"imooc.com/demo-lottery/web/middleware/identity"
	"imooc.com/demo-lottery/web/routes"
)

var port = 8080

func newApp() *bootstrap.Bootstrapper {
	// 初始化应用
	app := bootstrap.New("Go抽奖系统", "你好程序员")
	app.Bootstrap()
	app.Configure(identity.Configure, routes.Configure)

	return app
}

func main() {
	app := newApp()
	app.Listen(fmt.Sprintf(":%d", port))
}
