package routes

import (
	"github.com/kataras/iris/mvc"
	"imooc.com/demo-lottery/bootstrap"
	"imooc.com/demo-lottery/services"
	"imooc.com/demo-lottery/web/controllers"
	"imooc.com/demo-lottery/web/middleware"
)

func Configure(b *bootstrap.Bootstrapper) {
	// 初始化6个engine
	userService := services.NewUserService()
	giftService := services.NewGiftService()
	codeService := services.NewCodeService()
	resultService := services.NewResultService()
	userdayService := services.NewUserdayService()
	blackipService := services.NewBlackipService()

	// 前端页面
	// http://localhost:8080
	index := mvc.New(b.Party("/"))
	index.Register(
		userService,
		giftService,
		codeService,
		resultService,
		userdayService,
		blackipService,
	)
	index.Handle(new(controllers.IndexController))

	// 后台管理页面
	// http://localhost:8080/admin
	admin := mvc.New(b.Party("/admin"))
	admin.Router.Use(middleware.BasicAuth)
	admin.Register(
		userService,
		giftService,
		codeService,
		resultService,
		userdayService,
		blackipService,
	)
	admin.Handle(new(controllers.AdminController))

	// 后台礼物页面
	// 从 admin 派生出来 http://localhost:8080/admin/gift
	// 所以权限相关的也会被复用下来，如 basicAuth 就不用再重复写
	adminGift := admin.Party("/gift")
	//adminGift.Router.Use(middleware.BasicAuth)
	//adminGift.Register(giftService)
	adminGift.Handle(new(controllers.AdminGiftController))

	adminCode := admin.Party("/code")
	//adminCode.Register(codeService)
	adminCode.Handle(new(controllers.AdminCodeController))

	adminResult := admin.Party("/result")
	//adminResult.Register(resultService)
	adminResult.Handle(new(controllers.AdminResultController))

	adminUser := admin.Party("/user")
	adminUser.Handle(new(controllers.AdminUserController))

	adminBlackip := admin.Party("/blackip")
	adminBlackip.Handle(new(controllers.AdminBlackipController))

	rpc := mvc.New(b.Party("/rpc"))
	rpc.Register(userService,
		giftService,
		codeService,
		resultService,
		userdayService,
		blackipService,
	)
	rpc.Handle(new(controllers.RpcController))
}
