package utils

import (
	"fmt"
	"imooc.com/demo-lottery/datasource"
)

const OK = "OK"

func getLuckyLockKey(uid int) string {
	return fmt.Sprintf("lucky_lock_%d", uid)
}

func LockLucky(uid int) bool {
	key := getLuckyLockKey(uid)
	cacheObj := datasource.InstanceCache()
	// nx：不存在才设置
	// ex 3：3秒后过期
	rs, _ := cacheObj.Do("SET", key, 1, "EX", 3, "NX")
	if rs == OK {
		return true
	} else {
		return false
	}
}

func UnlockLucky(uid int) bool {
	key := getLuckyLockKey(uid)
	cacheObj := datasource.InstanceCache()
	rs, _ := cacheObj.Do("DEL", key)

	if rs == OK {
		return true
	} else {
		return false
	}
}
