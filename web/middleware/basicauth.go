package middleware

import (
	"github.com/kataras/iris/middleware/basicauth"
	"imooc.com/demo-lottery/conf"
)

var BasicAuth = basicauth.New(basicauth.Config{
	Users: map[string]string{
		conf.ADMIN: conf.PASSWORD, //后台管理的用户名字和密码
	},
})
