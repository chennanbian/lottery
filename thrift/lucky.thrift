#命名空间
namespace go rpc
namespace php rpc
namespace java rpc

# 数据结构：奖品信息
struct DataGiftPrize{
    1: i64 Id           = 0
    2: string Title     = ""
    3: string Img       = ""
    4: i64 Displayorder = 0
    5: i64 Gtype        = 0
    6: string Gdata     = ""
}

# 数据结构：返回值
struct DataResult{
    1: i64 code
    2: string Msg
    3: DataGiftPrize Gift
}

# 服务接口
service LuckyService {
    #抽奖的方法
    #DataResult: 返回的结果
    DataResult DoLucky(1:i64 uid, 2:string username,
    3:string ip, 4:i64 now, 5:string app 6:string sign),

    #list<DataGiftPrize>: 返回的结果，DataGiftPrize的列表，有点像数组
    list<DataGiftPrize> MyPrizeList(1:i64 uid, 2:string username,
    3:string ip, 4:i64 now, 5:string app 6:string sign)
}
