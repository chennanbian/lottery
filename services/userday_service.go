package services

import (
	"fmt"
	"imooc.com/demo-lottery/dao"
	"imooc.com/demo-lottery/datasource"
	"imooc.com/demo-lottery/models"
	"strconv"
	"time"
)

type UserdayService interface {
	Get(id int) *models.LtUserday
	GetAll(page, size int) []models.LtUserday
	Search(uid, day int) []models.LtUserday
	Count(uid, day int) int
	Update(data *models.LtUserday, cols []string) error
	Create(data *models.LtUserday) error
	GetUserToday(uid int) *models.LtUserday
}

type userdayService struct {
	dao *dao.UserDayDao
}

func NewUserdayService() UserdayService {
	return &userdayService{
		dao: dao.NewUserDayDao(datasource.InstanceDbMaster()),
	}
}

func (s *userdayService) Get(id int) *models.LtUserday {
	return s.dao.Get(id)
}

func (s *userdayService) GetAll(page, size int) []models.LtUserday {
	return s.dao.GetAll(page, size)
}

func (s *userdayService) Search(uid, day int) []models.LtUserday {
	return s.dao.Search(uid, day)
}

func (s *userdayService) Count(uid, day int) int {
	return s.dao.Count(uid, day)
}

func (s *userdayService) Update(data *models.LtUserday, cols []string) error {
	return s.dao.Update(data, cols)
}

func (s *userdayService) Create(data *models.LtUserday) error {
	return s.dao.Create(data)
}

func (c *userdayService) GetUserToday(uid int) *models.LtUserday {
	y, m, d := time.Now().Date()
	// %02d: 10进制输出，不足2位前面补0，如5，输出是05
	strDay := fmt.Sprintf("%d%02d%02d", y, m, d)
	day, _ := strconv.Atoi(strDay)
	list := c.dao.Search(uid, day)
	if list != nil && len(list) > 0 {
		return &list[0]
	}
	return nil
}
