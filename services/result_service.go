package services

import (
	"imooc.com/demo-lottery/dao"
	"imooc.com/demo-lottery/datasource"
	"imooc.com/demo-lottery/models"
)

type ResultService interface {
	Get(id int) *models.LtResult
	GetAll(page, size int) []models.LtResult
	CountAll() int64
	GetNewPrize(size int, giftIds []int) []models.LtResult
	SearchByGift(giftId, page, size int) []models.LtResult
	SearchByUser(uid, page, size int) []models.LtResult
	CountByGift(giftId int) int64
	CountByUser(uid int) int64
	Delete(id int) error
	Update(data *models.LtResult, cols []string) error
	Create(data *models.LtResult) error
}

type resultService struct {
	dao *dao.ResultDao
}

func NewResultService() ResultService {
	return &resultService{
		dao: dao.NewResultDao(datasource.InstanceDbMaster()),
	}
}

func (s *resultService) Get(id int) *models.LtResult {
	return s.dao.Get(id)
}

func (s *resultService) GetAll(page, size int) []models.LtResult {
	return s.dao.GetAll(page, size)
}

func (s *resultService) CountAll() int64 {
	return s.dao.CountAll()
}

func (s *resultService) GetNewPrize(size int, giftIds []int) []models.LtResult {
	return s.dao.GetNewPrize(size, giftIds)
}

func (s *resultService) SearchByGift(giftId, page, size int) []models.LtResult {
	return s.dao.SearchByGift(giftId, page, size)
}

func (s *resultService) SearchByUser(uid, page, size int) []models.LtResult {
	return s.dao.SearchByUser(uid, page, size)
}

func (s *resultService) CountByGift(giftId int) int64 {
	return s.dao.CountByGift(giftId)
}

func (s *resultService) CountByUser(uid int) int64 {
	return s.dao.CountByUser(uid)
}

func (s *resultService) Delete(id int) error {
	return s.dao.Delete(id)
}

func (s *resultService) Update(data *models.LtResult, cols []string) error {
	return s.dao.Update(data, cols)
}

func (s *resultService) Create(data *models.LtResult) error {
	return s.dao.Create(data)
}
