package dao

import (
	"github.com/go-xorm/xorm"
	"imooc.com/demo-lottery/models"
	"log"
)

type UserDao struct {
	engine *xorm.Engine
}

func NewUserDao(engine *xorm.Engine) *UserDao {
	return &UserDao{engine: engine}
}

func (d *UserDao) Get(id int) *models.LtUser {
	data := &models.LtUser{Id: id}
	ok, err := d.engine.Get(data)
	if ok && err == nil {
		return data
	} else {
		data.Id = 0
		return data
	}
}

func (d *UserDao) GetAll(page, size int) []models.LtUser {
	offset := (page - 1) * size
	datalist := []models.LtUser{}
	err := d.engine.
		Desc("id").
		Limit(size, offset).
		Find(&datalist)
	if err != nil {
		log.Println("user_dao.GetAll error=", err)
	}
	return datalist
}

func (d *UserDao) CountAll() int64 {
	// models.LtUser的非空字段是条件，这里都是空就是无条件，查全部
	num, err := d.engine.Count(&models.LtUser{})
	if err != nil {
		return 0
	}
	return num
}

// 结构体内某个字段若是空的，默认不会将空值更新进去mysql，要强制更新用mustCols
// mustCols: 强制要求哪些字段一定要更新
func (d *UserDao) Update(data *models.LtUser, cols []string) error {
	_, err := d.engine.Id(data.Id).MustCols(cols...).Update(data)
	return err
}

func (d *UserDao) Create(data *models.LtUser) error {
	_, err := d.engine.Insert(data)
	return err
}
