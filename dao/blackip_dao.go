package dao

import (
	"github.com/go-xorm/xorm"
	"imooc.com/demo-lottery/models"
	"log"
)

type BlackIpDao struct {
	engine *xorm.Engine
}

func NewBlackIpDao(engine *xorm.Engine) *BlackIpDao {
	return &BlackIpDao{engine: engine}
}

func (d *BlackIpDao) Get(id int) *models.LtBlackip {
	data := &models.LtBlackip{Id: id}
	ok, err := d.engine.Get(data)
	if ok && err == nil {
		return data
	} else {
		data.Id = 0
		return data
	}
}

// page:第几页  size：取多少数量
func (d *BlackIpDao) GetAll(page, size int) []models.LtBlackip {
	offset := (page - 1) * size
	datalist := []models.LtBlackip{}
	err := d.engine.
		Desc("id").
		// 限制获取的数目，第一个参数为条数，第二个参数表示开始位置，如果不传则为0
		Limit(size, offset).
		Find(&datalist)
	if err != nil {
		log.Println("blackip_dao.GetAll error=", err)
	}
	return datalist
}

func (d *BlackIpDao) CountAll() int64 {
	// models.LtBlackip的非空字段是条件，这里都是空就是无条件，查全部
	num, err := d.engine.Count(&models.LtBlackip{})
	if err != nil {
		return 0
	}
	return num
}

func (d *BlackIpDao) Search(ip string) []models.LtBlackip {
	// 软删除，只是改变状态，定义 1 表示不可用
	datalist := []models.LtBlackip{}
	err := d.engine.
		Where("ip=?", ip).
		Desc("id").
		Find(&datalist)
	if err != nil {
		log.Println("blackip_dao.Search error=", err)
	}
	return datalist
}

// 结构体内某个字段若是空的，默认不会将空值更新进去mysql，要强制更新用mustCols
// mustCols: 强制要求哪些字段一定要更新
func (d *BlackIpDao) Update(data *models.LtBlackip, cols []string) error {
	_, err := d.engine.Id(data.Id).MustCols(cols...).Update(data)
	return err
}

func (d *BlackIpDao) Create(data *models.LtBlackip) error {
	_, err := d.engine.Insert(data)
	return err
}

func (d *BlackIpDao) GetByIp(ip string) *models.LtBlackip {
	data := &models.LtBlackip{Ip: ip}
	ok, err := d.engine.Get(data)
	if !ok || err != nil {
		log.Println("blackip_dao.GetByIp error=", err)
		return nil
	}
	return data
}
