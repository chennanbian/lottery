package dao

import (
	"github.com/go-xorm/xorm"
	"imooc.com/demo-lottery/comm"
	"imooc.com/demo-lottery/models"
	"log"
)

type GiftDao struct {
	engine *xorm.Engine
}

func NewGiftDao(engine *xorm.Engine) *GiftDao {
	return &GiftDao{engine: engine}
}

func (d *GiftDao) Get(id int) *models.LtGift {
	data := &models.LtGift{Id: id}
	ok, err := d.engine.Get(data)
	if ok && err == nil {
		return data
	} else {
		data.Id = 0
		return data
	}
}

func (d *GiftDao) GetAll() []models.LtGift {
	datalist := []models.LtGift{}
	err := d.engine.
		Asc("sys_status").
		Asc("displayorder").
		Find(&datalist)
	if err != nil {
		log.Println("gift_dao.GetAll error=", err)
	}
	return datalist
}

func (d *GiftDao) CountAll() int64 {
	// models.ltgift的非空字段是条件，这里都是空就是无条件，查全部
	num, err := d.engine.Count(&models.LtGift{})
	if err != nil {
		return 0
	}
	return num
}

func (d *GiftDao) Delete(id int) error {
	// 软删除，只是改变状态，定义 1 表示不可用
	data := &models.LtGift{Id: id, SysStatus: 1}
	_, err := d.engine.Id(data.Id).Update(data)
	return err
}

// 结构体内某个字段若是空的，默认不会将空值更新进去mysql，要强制更新用mustCols
// mustCols: 强制要求哪些字段一定要更新
func (d *GiftDao) Update(data *models.LtGift, cols []string) error {
	_, err := d.engine.Id(data.Id).MustCols(cols...).Update(data)
	return err
}

func (d *GiftDao) Create(data *models.LtGift) error {
	_, err := d.engine.Insert(data)
	return err
}

// 获取到当前可以获取的奖品列表
// 有奖品限定，状态正常，时间期间内
// gtype倒序， displayorder正序
func (d *GiftDao) GetAllUse() []models.LtGift {
	now := comm.NowUnix()
	datalist := make([]models.LtGift, 0)
	err := d.engine.
		Cols("id", "title", "prize_num", "left_num", "prize_code",
						"prize_time", "img", "displayorder", "gtype", "gdata").
		Desc("gtype").               //大奖派前面
		Asc("displayorder").         //序号小的派前面
		Where("sys_status=?", 0).    // 有效的奖品
		Where("prize_num>=?", 0).    // 有数量的奖品
		Where("time_begin<=?", now). // 时间期内
		Where("time_end>=?", now).   // 时间期内
		Find(&datalist)
	if err != nil {
		log.Println("gift_dao.GetAllUser error=", err)
	}
	return datalist
}

// 剩余数量增加+1
func (d *GiftDao) IncrLeftNum(id, num int) (int64, error) {
	r, err := d.engine.Id(id).
		// left_num=left_num + num
		Incr("left_num", num).
		//Where("left_num>?", 0).
		Update(&models.LtGift{Id: id})
	return r, err
}

// 剩余数量减少1
func (d *GiftDao) DecrLeftNum(id, num int) (int64, error) {
	r, err := d.engine.Id(id).
		// left_num=left_num - num
		Decr("left_num", num).
		// 比num大才减，不然有负值
		// 不过老师应该定义为非负数，这样就不会有负值
		// 具体的我测试过，如果定义为非负值，a=0 令 a=a-1
		// 会报错，具体看博客xorm，
		// 这里就不改了，下次自己做项目记得就行
		Where("left_num>=?", num).
		Update(&models.LtGift{Id: id})
	return r, err
}
