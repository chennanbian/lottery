package dao

import (
	"github.com/go-xorm/xorm"
	"imooc.com/demo-lottery/models"
	"log"
)

type ResultDao struct {
	engine *xorm.Engine
}

func NewResultDao(engine *xorm.Engine) *ResultDao {
	return &ResultDao{engine: engine}
}

func (d *ResultDao) Get(id int) *models.LtResult {
	data := &models.LtResult{Id: id}
	ok, err := d.engine.Get(data)
	if ok && err == nil {
		return data
	} else {
		data.Id = 0
		return data
	}
}

func (d *ResultDao) GetAll(page, size int) []models.LtResult {
	offset := (page - 1) * size
	datalist := []models.LtResult{}
	err := d.engine.
		Desc("id").
		Limit(size, offset).
		Find(&datalist)
	if err != nil {
		log.Println("result_dao.GetAll error=", err)
	}
	return datalist
}

func (d *ResultDao) CountAll() int64 {
	// models.LtResult的非空字段是条件，这里都是空就是无条件，查全部
	num, err := d.engine.Count(&models.LtResult{})
	if err != nil {
		return 0
	}
	return num
}

// 获取gift_id最新size个获奖信息
func (d *ResultDao) GetNewPrize(size int, giftIds []int) []models.LtResult {
	datalist := make([]models.LtResult, 0)
	err := d.engine.
		In("gift_id", giftIds).
		Desc("id").
		Limit(size).
		Find(&datalist)
	if err != nil {
		log.Println("result_dao.GetNewPrize error=", err)
	}
	return datalist
}

// 获取第几页的奖品信息
func (d *ResultDao) SearchByGift(giftId, page, size int) []models.LtResult {
	offset := (page - 1) * size
	datalist := make([]models.LtResult, 0)
	err := d.engine.
		Where("gift_id=?", giftId).
		Desc("id").
		Limit(size, offset).
		Find(&datalist)
	if err != nil {
		log.Println("result_dao.SearchByGift error=", err)
	}
	return datalist
}

// 查找用户第几页的获奖信息
func (d *ResultDao) SearchByUser(uid, page, size int) []models.LtResult {
	offset := (page - 1) * size
	datalist := make([]models.LtResult, 0)
	err := d.engine.
		Where("uid=?", uid).
		Desc("id").
		Limit(size, offset).
		Find(&datalist)
	if err != nil {
		log.Println("result_dao.SearchByUser error=", err)
	}
	return datalist
}

// gift_id总共发出去多少奖
func (d *ResultDao) CountByGift(giftId int) int64 {
	num, err := d.engine.
		Where("gift_id=?", giftId).
		Count(&models.LtResult{})
	if err != nil {
		log.Println("result_dao.CountByGift error=", err)
		return 0
	}
	return num
}

// 用户总共获取多少次奖
func (d *ResultDao) CountByUser(uid int) int64 {
	num, err := d.engine.
		Where("uid=?", uid).
		Count(&models.LtResult{})
	if err != nil {
		log.Println("result_dao.CountByUser error=", err)
		return 0
	}
	return num
}

func (d *ResultDao) Delete(id int) error {
	data := &models.LtResult{Id: id, SysStatus: 1}
	_, err := d.engine.Id(data.Id).Update(data)
	return err
}

// 结构体内某个字段若是空的，默认不会将空值更新进去mysql，要强制更新用mustCols
// mustCols: 强制要求哪些字段一定要更新
func (d *ResultDao) Update(data *models.LtResult, cols []string) error {
	_, err := d.engine.Id(data.Id).MustCols(cols...).Update(data)
	return err
}

func (d *ResultDao) Create(data *models.LtResult) error {
	_, err := d.engine.Insert(data)
	return err
}
