package dao

import (
	"github.com/go-xorm/xorm"
	"imooc.com/demo-lottery/models"
	"log"
)

type CodeDao struct {
	engine *xorm.Engine
}

func NewCodeDao(engine *xorm.Engine) *CodeDao {
	return &CodeDao{engine: engine}
}

func (d *CodeDao) Get(id int) *models.LtCode {
	data := &models.LtCode{Id: id}
	ok, err := d.engine.Get(data)
	if ok && err == nil {
		return data
	} else {
		data.Id = 0
		return data
	}
}

func (d *CodeDao) GetAll(page, size int) []models.LtCode {
	datalist := []models.LtCode{}
	offset := (page - 1) * size
	err := d.engine.
		Desc("id").
		Limit(size, offset).
		Find(&datalist)
	if err != nil {
		log.Println("code_dao.GetAll error=", err)
	}
	return datalist
}

func (d *CodeDao) CountAll() int64 {
	// models.LtCode的非空字段是条件，这里都是空就是无条件，查全部
	num, err := d.engine.Count(&models.LtCode{})
	if err != nil {
		return 0
	}
	return num
}

func (d *CodeDao) CountByGift(giftId int) int64 {
	num, err := d.engine.
		Where("gift_id=?", giftId).
		Count(&models.LtCode{})
	if err != nil {
		log.Println("code_dao.CountByGift error=", err)
		return 0
	}
	return num
}

func (d *CodeDao) Search(giftId int) []models.LtCode {
	datalist := []models.LtCode{}
	err := d.engine.
		Where("gift_id=?", giftId).
		Desc("id").
		Find(&datalist)
	if err != nil {
		log.Println("code_dao.Search error=", err)
	}
	return datalist
}

func (d *CodeDao) Delete(id int) error {
	// 软删除，只是改变状态，定义 1 表示不可用
	data := &models.LtCode{Id: id, SysStatus: 1}
	_, err := d.engine.Id(data.Id).Update(data)
	return err
}

// 结构体内某个字段若是空的，默认不会将空值更新进去mysql，要强制更新用mustCols
// mustCols: 强制要求哪些字段一定要更新
func (d *CodeDao) Update(data *models.LtCode, cols []string) error {
	_, err := d.engine.Id(data.Id).MustCols(cols...).Update(data)
	return err
}

func (d *CodeDao) Create(data *models.LtCode) error {
	_, err := d.engine.Insert(data)
	return err
}

// 找到下一个可用的最小的优惠券
func (d *CodeDao) NextUsingCode(giftId, codeId int) *models.LtCode {
	datalist := make([]models.LtCode, 0)
	err := d.engine.Where("gift_id=?", giftId).
		Where("sys_status=?", 0).
		Where("id>?", codeId).
		Asc("id").Limit(1).
		Find(&datalist)
	if err != nil || len(datalist) < 1 {
		return nil
	} else {
		return &datalist[0]
	}
}

// 根据唯一的code来更新
func (d *CodeDao) UpdateByCode(data *models.LtCode, columns []string) error {
	_, err := d.engine.Where("code=?", data.Code).
		MustCols(columns...).Update(data)
	return err
}
